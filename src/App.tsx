import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import {
  LineChart,
  Legend,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import {
  Container,
  Row,
  Col,
  Image,
  ButtonGroup,
  ToggleButton,
} from "react-bootstrap";
import arrowDown from "./images/iconfinder_Stock Index Down_27880_small.png";
import arrowUp from "./images/iconfinder_Stock Index Up_27881_small.png";
import arrowNeutral from "./images/iconfinder_ic_trending_neutral_48px_352183.png";
import gp from "./images/runescape-07-gold-icon.png";
import sampleJson from "./sample.json";
// const data = [{ name: "Page A", uv: 400, pv: 2400, amt: 2400 }];

function App() {
  const [graphResult, setGraphResult] = useState<any[]>([
    { status: "loading" },
  ]);
  const [detailResult, setDetailResult] = useState<any>(sampleJson);
  const [search, setSearch] = useState<string>("383");
  const [radioValue, setRadioValue] = useState("itemdb_oldschool");

  const graphUrl = `https://services.runescape.com/m=${radioValue}/api/graph/${search}.json`;
  const detailUrl = `https://secure.runescape.com/m=${radioValue}/api/catalogue/detail.json?item=${search}`;
  const imageUrl = `https://secure.runescape.com/m=${radioValue}/obj_big.gif?id=${search}`;

  const radios = [
    { name: "OSRS", value: "itemdb_oldschool" },
    { name: "RS3", value: "itemdb_rs" },
  ];

  function getMinPrice(data: { time: string; price: number }[]) {
    return data.reduce(
      (min, p) => (p.price < min ? p.price : min),
      data[0].price
    );
  }
  function getMaxPrice(data: { time: string; price: number }[]) {
    return data.reduce(
      (max, p) => (p.price > max ? p.price : max),
      data[0].price
    );
  }

  useEffect(() => {
    fetch(graphUrl)
      .then((response) => response.json())
      .then((response) => {
        const keys: any[] = [];
        Object.keys(response.daily).forEach((key) =>
          keys.push({
            time: new Date(Number(key)).toLocaleDateString(),
            price: Number(response.daily[key]),
          })
        );
        setGraphResult(keys);
      })
      .catch((error) => console.log(error));

    fetch(detailUrl)
      .then((response) => response.json())
      .then((response) => {
        setDetailResult(response);
      })
      .catch((error) => console.log(error));
  }, [detailUrl, graphUrl]);

  const handleSubmit = (event: any) => {
    setSearch(event.currentTarget.name.value);
    event?.preventDefault();
  };

  const trendImage = (trend: string): string => {
    switch (trend) {
      case "neutral":
        return arrowNeutral;
      case "positive":
        return arrowUp;
      case "negative":
        return arrowDown;
      default:
        return arrowNeutral;
    }
  };

  return (
    <div
      className="App"
      style={{
        alignContent: "center",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <form onSubmit={handleSubmit}>
        <label>
          Search by Item ID
          <input type="text" name="name" />
        </label>
        <input type="submit" value="submit" />
      </form>
      <ButtonGroup toggle>
        {radios.map((radio, idx) => (
          <ToggleButton
            key={idx}
            type="radio"
            variant="secondary"
            name="radio"
            value={radio.value}
            checked={radioValue === radio.value}
            onChange={(e) => setRadioValue(e.currentTarget.value)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
      <h1>
        Price of {detailResult.item.name} (id: {detailResult.item.id}) over the
        past 180 days
      </h1>
      <Image src={imageUrl} />
      <h3>{detailResult.item.description}</h3>
      <Container fluid>
        <Row>
          <Col xs={6} md={2}>
            <p>Current Price: {detailResult.item.current.price} gp</p>
            <Image src={gp} width="48px" />
          </Col>
          <Col xs={6} md={2}>
            <p>Today: {detailResult.item.today.price} gp</p>
            <Image src={trendImage(detailResult.item.today.trend)} />
          </Col>
          <Col xs={6} md={2}>
            <p>30 Day Trend: {detailResult.item.day30.change}</p>
            <Image src={trendImage(detailResult.item.day30.trend)} />
          </Col>
          <Col xs={6} md={2}>
            <p>90 Day Trend: {detailResult.item.day90.change}</p>
            <Image src={trendImage(detailResult.item.day90.trend)} />
          </Col>
          <Col xs={6} md={2}>
            <p>180 Day Trend: {detailResult.item.day180.change}</p>
            <Image src={trendImage(detailResult.item.day180.trend)} />
          </Col>
          <Col xs={6} md={2}>
            <p>Lowest Price: {getMinPrice(graphResult)}</p>
            <p>Hightest Price: {getMaxPrice(graphResult)}</p>
          </Col>
        </Row>
      </Container>
      <ResponsiveContainer width="80%" height={500}>
        <LineChart
          data={graphResult}
          margin={{ top: 5, right: 0, left: 300, bottom: 5 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="time" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="price" stroke="#0095FF" />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}

export default App;
